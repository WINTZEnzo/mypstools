# MyPSTOOLS
## Objectifs du projet
Ce projet a pour but de fournir un outil en Powershell avec la possibilité de pouvoir compiler ce dernier, le tout avec une méthodologie DevOps sur un environnement exclusivement dédié sur Windows.

## Contexte du projet
Initialement, toutes les étapes de ce projet se faisait avec plusieurs interractions et plusieurs manipulations sur le poste où les sources se trouvait pour pouvoir créer l'exécutable en bout de chaine.

Étant issu d'une formation DevOps, j'ai pour ambition d'amener plus que la culture DevOps et cela passe par l'utilisation d'Ansible afin de pouvoir __Industrialiser__ toutes les étapes de ce projet et ainsi limiter les opérations manuelles humaines.

## Technologies utilisées pour le projet
PSTOOLS offre la possibilité d'utiliser des scripts en **powershell** et en **XML** (la combinaison des deux étant du **WPF**).

### Ansible
La cohérence DevOps de ce projet est que ce projet se base sur l'utilisation de **Ansible**.

**Ansible** est donc utilisé afin d'employer le système de variabilisation que nous met à disposition cette technologie.

Ainsi, nous y retrouvons deux rôles : 
- Un rôle **pstools** regroupant l'ensemble des fichiers et les différentes configurations afin de pouvoir créer l'exécutable qui sera utilisé sur les différents postes.

- Un rôle **runner** qui permettra la configuration d'un runner utilisé par Gitlab dans le cadre d'un déploiement automatisé via l'utilisation d'une chaine CI/CD.

### Gitlab
Afin de ne pas avoir avoir à devoir taper les commandes Ansible manuellement, l'utilisation de **Gitlab** est nécessaire permettant ainsi d'employer les différents stages en passant par le début de la configuration pour finir par la création de notre exécutable __PSTOOLS__.

C'est ainsi qu'au sein de notre chaine CI/CD, nous retrouvant les étapes suivantes : 
- **Préparation des sources :** On prépare nos sources dans notre dossier **COMPILE2EXE** au sein du dossier __files__ du rôle Ansible __pstools__.
- **Copie des sources sur notre host Windows :** On copie les sources du dossier **COMPILE2EXE** préparées précédemment en local sur notre host Windows.
- **Compilation des sources + Création de notre exécutable :** On va créer notre exécutable avec l'utilisation de l'outil **IEXPRESS** (outil intégré nativement sur les systèmes Windows mais pas sur les Windows tournant sous __ARM__ notamment en tant que machine virtuelle).

### Powershell et XML
Notre exécutable final ne pourrait pas se lancer sans l'utilisation notre script d'interface en **Powershell**.

L'utilisation de **Powershell** se fait dans le cadre de pouvoir être utilisé sur des postes exclusivement sous Windows.

C'est ainsi qu'au sein de notre exécutable, nous retrouvons : 
- Notre script d'interface principale (__.ps1__) qui fera appel à un ensemble de ressources.
- Notre deux fichiers __.xaml__ permettant de savoir quelle interface doit être lancé lors du lancement de notre outil.
- Nos différentes Images qui composeront les différents menus de l'outil.
- Notre __.dll__ permettant de pouvoir appliquer un effet de **Menu Radial** à notre outil.
- L'ensemble des scripts qui seront enclenchés lors des différentes interractions poussées lors d'exécution d'actions spécifique.

### Arborescence du projet
```bash
.
├── README.md
├── ansible
│   ├── README.md
│   ├── ansible.cfg
│   ├── artefacts
│   │   └── README.md
│   ├── group_vars
│   │   ├── all.yml
│   │   └── windows.yml
│   ├── hosts
│   ├── playbooks
│   │   ├── pstools
│   │   │   ├── pstools_localhost.yml
│   │   │   └── pstools_windows.yml
│   │   └── runner
│   │       └── runner.yml
│   ├── roles
│   │   ├── pstools
│   │   │   ├── README.md
│   │   │   ├── defaults
│   │   │   │   └── main.yml
│   │   │   ├── files
│   │   │   │   ├── COMPIL2EXE
│   │   │   │   │   ├── CopyDataToCTravail.ps1
│   │   │   │   │   ├── Icons.xaml
│   │   │   │   │   ├── MajPoliciesSCCM.ps1
│   │   │   │   │   ├── Octocat.png
│   │   │   │   │   ├── PSTOOLS2-VERSION1.xaml
│   │   │   │   │   ├── PSTOOLS2-VERSION2.xaml
│   │   │   │   │   ├── PSTOOLS2.ps1
│   │   │   │   │   ├── PURGE_REGISTRY.ps1
│   │   │   │   │   ├── PurgeCacheSCCM.ps1
│   │   │   │   │   ├── PurgeProfilV4.ps1
│   │   │   │   │   ├── RadialMenu.dll
│   │   │   │   │   ├── UninstallClientSCCM.ps1
│   │   │   │   │   ├── VerrNum.png
│   │   │   │   │   ├── VerrNum_BarreDeProgression.ps1
│   │   │   │   │   ├── aide.png
│   │   │   │   │   ├── appli.png
│   │   │   │   │   ├── cachesccm.png
│   │   │   │   │   ├── centre_logiciel.png
│   │   │   │   │   ├── cmtrace.exe
│   │   │   │   │   ├── delete.png
│   │   │   │   │   ├── dico.png
│   │   │   │   │   ├── explorer.png
│   │   │   │   │   ├── home.png
│   │   │   │   │   ├── install.png
│   │   │   │   │   ├── interro.png
│   │   │   │   │   ├── java.png
│   │   │   │   │   ├── java_uninstall.ps1
│   │   │   │   │   ├── java_uninstallWithProgressBar.ps1
│   │   │   │   │   ├── jmk.png
│   │   │   │   │   ├── keepass.png
│   │   │   │   │   ├── purge_profil.png
│   │   │   │   │   ├── refresh.png
│   │   │   │   │   ├── regedit.png
│   │   │   │   │   ├── return.png
│   │   │   │   │   ├── sccm_logo.png
│   │   │   │   │   ├── user.png
│   │   │   │   │   └── windows.png
│   │   │   │   ├── assembly
│   │   │   │   │   └── RadialMenu.dll
│   │   │   │   ├── icones
│   │   │   │   │   ├── Octocat.png
│   │   │   │   │   ├── VerrNum.png
│   │   │   │   │   ├── aide.png
│   │   │   │   │   ├── appli.png
│   │   │   │   │   ├── cachesccm.png
│   │   │   │   │   ├── centre_logiciel.png
│   │   │   │   │   ├── delete.png
│   │   │   │   │   ├── dico.png
│   │   │   │   │   ├── explorer.png
│   │   │   │   │   ├── home.png
│   │   │   │   │   ├── install.png
│   │   │   │   │   ├── interro.png
│   │   │   │   │   ├── java.png
│   │   │   │   │   ├── jmk.png
│   │   │   │   │   ├── keepass.png
│   │   │   │   │   ├── purge_profil.png
│   │   │   │   │   ├── refresh.png
│   │   │   │   │   ├── regedit.png
│   │   │   │   │   ├── return.png
│   │   │   │   │   ├── sccm_logo.png
│   │   │   │   │   ├── user.png
│   │   │   │   │   └── windows.png
│   │   │   │   ├── sed
│   │   │   │   │   └── PSTOOLS2.SED
│   │   │   │   └── utilitaires
│   │   │   │       └── cmtrace.exe
│   │   │   ├── tasks
│   │   │   │   ├── copy_win_sources.yml
│   │   │   │   ├── main.yml
│   │   │   │   ├── prepare_sed.yml
│   │   │   │   ├── prepare_sources_localhost.yml
│   │   │   │   └── sed_and_compil.yml
│   │   │   ├── templates
│   │   │   │   ├── interface
│   │   │   │   │   └── PSTOOLS2.ps1.j2
│   │   │   │   ├── ressources
│   │   │   │   │   └── Icons.xaml.j2
│   │   │   │   ├── scripts
│   │   │   │   │   ├── LOCALHOST_APPLICATIONS
│   │   │   │   │   │   ├── java_uninstall.ps1.j2
│   │   │   │   │   │   └── java_uninstallWithProgressBar.ps1.j2
│   │   │   │   │   ├── SCCM
│   │   │   │   │   │   ├── MajPoliciesSCCM.ps1.j2
│   │   │   │   │   │   ├── PURGE_REGISTRY.ps1.j2
│   │   │   │   │   │   ├── PurgeCacheSCCM.ps1.j2
│   │   │   │   │   │   └── UninstallClientSCCM.ps1.j2
│   │   │   │   │   └── WINDOWS
│   │   │   │   │       ├── CopyDataToCTravail.ps1.j2
│   │   │   │   │       ├── PurgeProfilV4.ps1.j2
│   │   │   │   │       └── VerrNum_BarreDeProgression.ps1.j2
│   │   │   │   ├── sed
│   │   │   │   │   └── PSTOOLS2.SED.j2
│   │   │   │   └── xaml
│   │   │   │       ├── PSTOOLS2-VERSION1.xaml.j2
│   │   │   │       └── PSTOOLS2-VERSION2.xaml.j2
│   │   │   └── vars
│   │   │       └── main.yml
│   │   └── runner
│   │       ├── README.md
│   │       ├── defaults
│   │       │   └── main.yml
│   │       ├── tasks
│   │       │   ├── build_container.yml
│   │       │   ├── dependances.yml
│   │       │   ├── main.yml
│   │       │   ├── prepare_files.yml
│   │       │   └── start_runner.yml
│   │       └── templates
│   │           ├── Dockerfile.j2
│   │           └── docker-compose.yml.j2
│   └── runner
│       └── README.md
├── ci-playbook
│   ├── compile_sources.yml
│   ├── copy_sources.yml
│   └── prepare_sources.yml
└── windows
    ├── README.md
    └── Setup_WinRM_Windows.ps1

33 directories, 109 files
```

