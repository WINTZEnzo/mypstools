Role runner
=========

Pour utiliser le module docker sur ansible : 
```bash
ansible-galaxy collection install community.docker
```

## Préparation du runner
Le playbook concerné par cette partie est ```runner.yml```.

On exécutera la commande suivante pour préparer les fichiers docker et le build de l'image : 
```bash
ansible-playbook -i hosts playbooks/runner/runner.yml --extra-vars "conf=build-runner" -u root -K --diff
```
## Start du runner
Pour rendre le runner opérationnel avec notre gitlab, nous taperons la commmande ansible suivante : 
```bash
ansible-playbook -i hosts playbooks/runner/runner.yml --extra-vars "conf=start-runner" -u root -K --diff
```