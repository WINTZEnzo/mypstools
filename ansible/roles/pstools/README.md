Role pstools
=========
# Préparation des sources
```bash
ansible-playbook -i hosts playbooks/pstools/pstools_localhost.yml --extra-vars "step=preparation" -u root -K --diff
```

Le ```-u root -K``` se joue uniquement si la commande est exécuté en local, dans le cadre de l'utilisation de notre runner, cette partie est retirée.

# Copie des sources sur notre host Windows
```bash
ansible-playbook -i hosts playbooks/pstools/pstools_windows.yml --extra-vars "step=copy" --diff 
```

# Import du fichier .SED + création de l'exécutable
```bash
ansible-playbook -i hosts playbooks/pstools/pstools_windows.yml --extra-vars "step=compilation" --diff 
```

## Debug en cas d'erreur connue
Si vous avez une erreur de type : 
```bash
objc[25990]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called.
objc[25990]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called. We cannot safely call it or ignore it in the fork() child process. Crashing instead. Set a breakpoint on objc_initializeAfterForkError to debug.
ERROR! A worker was found in a dead state
```
Il faudra taper la commande suivante dans le terminal puis rejouer la commande ansible : 
```bash
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```