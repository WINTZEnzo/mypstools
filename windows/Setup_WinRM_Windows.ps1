clear
#Preparation de l'hôte Windows pour être un host administré par Ansible
write-host "Debut du script d'installation de l'hote $env:COMPUTERNAME"

$Global:arbo_base = "HKLM:\SOFTWARE"
$Global:dir_ansible = "ANSIBLE"
$Global:arbo_ansible = "$Global:arbo_base\$Global:dir_ansible"
$Global:username = "$env:USERNAME"
$Global:password = read-host "Quelle est votre mot de passe ?" -AsSecureString

#Fonction de cr�ation de cl� de registre � chaque �tape du script
function Set-RegistryKeyValue {
   param ($Key_dir)

if ($Key_dir -eq $Global:dir_ansible)
    {
    if (-not(test-path $Global:arbo_ansible))
        {
        write-host "Creation de la cle de registre $Global:arbo_base\$Key_dir" -ForegroundColor Yellow
        New-Item -Path $Global:arbo_base -Name "$Key_dir" | Out-Null -ErrorAction SilentlyContinue
        }
    else {write-host "La cle de registre $Global:arbo_base\$Key_dir existe deja" -ForegroundColor green}
    }
elseif ($Key_dir -eq "PURGE")
     {
    if (test-path $Global:arbo_ansible)
        {
         write-host "Purge de la ruche $Global:arbo_ansible" -ForegroundColor Yellow
         Remove-Item -Path $Global:arbo_ansible -Recurse -ErrorAction SilentlyContinue
        }
    else {write-host "La cle de registre $Global:arbo_ansible n existe pas" -ForegroundColor green}
     }
else {
    if (-not(test-path "$Global:arbo_ansible\$Key_dir"))
        {
          write-host "Creation de la sous cle de registre $Global:arbo_ansible\$Key_dir" -ForegroundColor Yellow
          New-ItemProperty -Path $Global:arbo_ansible -name �$Key_dir� -Value �OK�  -PropertyType "String" | Out-Null -ErrorAction SilentlyContinue
        }
    else {write-host "La cle de registre $Global:arbo_ansible\$Key_dir existe deja" -ForegroundColor green}
     }
}
#On cr�� l'arborescence ANSIBLE dans notre registre
Set-RegistryKeyValue -Key_dir "ANSIBLE"
#Purge de l'arborescence ANSIBLE dans le registre
#Set-RegistryKeyValue -Key_dir "PURGE"

#Modification de la connexion r�seau pour qu'elle soit en priv�
function modify_network {
$network = @(Get-NetConnectionProfile)
$network_name = $network.name
$category = $network.NetworkCategory
$Type_not_wanted = "Public"
$Type_wanted = "Private"

if ($category -eq "$Type_not_wanted")
    {
    write-host "change $Type_not_wanted for $network_name" -ForegroundColor green
    Set-NetConnectionProfile -Name "$network_name" -NetworkCategory $Type_wanted
    }
else {write-host "$network_name already compliant : $Type_wanted" -ForegroundColor white}
}


#fonction Upgrading PowerShell and .NET Framework
function upgrade_framework {
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1"
$file = "$env:temp\Upgrade-PowerShell.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force

# Version can be 3.0, 4.0 or 5.1
&$file -Version 5.1 -Username $Global:username -Password $Global:password -Verbose
}

#fonction Remove auto logon
function remove_auto_logon {
# This isn't needed but is a good security practice to complete
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force

$reg_winlogon_path = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
Set-ItemProperty -Path $reg_winlogon_path -Name AutoAdminLogon -Value 0
Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultUserName -ErrorAction SilentlyContinue
Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultPassword -ErrorAction SilentlyContinue
}

#Fonction hotfix for WinRM
function winrm_fix {
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Install-WMF3Hotfix.ps1"
$file = "$env:temp\Install-WMF3Hotfix.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file -Verbose
}

#Setup WinRM
function winrm_setup {
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)

powershell.exe -ExecutionPolicy ByPass -File $file

Set-Service -Name "WinRM" -StartupType Automatic -Status Running

netsh advfirewall firewall add rule name="WinRM-HTTP" dir=in localport=5985 protocol=TCP action=allow

Set-Item WSMan:\localhost\Client\TrustedHosts * -Force
set-item WSMan:\localhost\Client\allowunencrypted $false
set-item WSMan:\localhost\service\allowunencrypted $true

winrm set winrm/config/client/auth '@{Basic="true"}'
winrm set winrm/config/service/auth '@{Basic="true"}'
winrm set winrm/config/client/auth '@{Certificate="true"}'
winrm set winrm/config/service/auth '@{Certificate="false"}'
winrm set winrm/config/client/auth '@{Kerberos="false"}'
winrm set winrm/config/service/auth '@{Kerberos="false"}'
restart-service -name winrm
}

###D�claration des variables pour le registre
$Network = "Network"
$Framework = "Framework"
$Logon = "Auto_logon"
$WinRM_Fix = "WinRM_Fix"
$WinRM_Setup = "WinRM_Setup"

if (-not(Get-ItemProperty -Path $Global:arbo_ansible | Select-Object -ExpandProperty $Network -ErrorAction SilentlyContinue))
   {
    write-host "modify network" -ForegroundColor Yellow
    Set-RegistryKeyValue -Key_dir "$Network"
    modify_network
    write-host "End modify network`n" -ForegroundColor green
   }
else {write-host "Partie $Network skip" -ForegroundColor Cyan}

if (-not(Get-ItemProperty -Path $Global:arbo_ansible | Select-Object -ExpandProperty $Framework -ErrorAction SilentlyContinue))
    {
    write-host "Upgrade du framework" -ForegroundColor Yellow
    Set-RegistryKeyValue -Key_dir "$Framework"
    upgrade_framework
    write-host "Fin de l'upgrade du framework`n" -ForegroundColor Green
    }
else {write-host "Partie $Framework skip" -ForegroundColor Cyan}

if (-not(Get-ItemProperty -Path $Global:arbo_ansible | Select-Object -ExpandProperty $Logon -ErrorAction SilentlyContinue))
    {
    write-host "Delete auto logon on registry" -ForegroundColor Yellow
    Set-RegistryKeyValue -Key_dir "$Logon"
    remove_auto_logon
    write-host "Fin de la suppression de l auto logon`n" -ForegroundColor Green
    }
else {write-host "Partie $Logon skip" -ForegroundColor Cyan}

if (-not(Get-ItemProperty -Path $Global:arbo_ansible | Select-Object -ExpandProperty $WinRM_Fix -ErrorAction SilentlyContinue))
    {
    write-host "Fix WinRM" -ForegroundColor Yellow
    Set-RegistryKeyValue -Key_dir "$WinRM_Fix"
    winrm_fix
    write-host "End fix WinRM`n" -ForegroundColor Green
    }
else {write-host "Partie $WinRM_Fix skip" -ForegroundColor Cyan}

if (-not(Get-ItemProperty -Path $Global:arbo_ansible | Select-Object -ExpandProperty $WinRM_Setup -ErrorAction SilentlyContinue))
    {
    write-host "Setup WinRM" -ForegroundColor Yellow
    Set-RegistryKeyValue -Key_dir "$WinRM_Setup"
    winrm_setup
    write-host "End Setup WinRM`n" -ForegroundColor Green
    }
else {write-host "Partie $WinRM_Setup skip" -ForegroundColor Cyan}

#Purge de l'arborescence ANSIBLE dans le registre
#Set-RegistryKeyValue -Key_dir "PURGE"

write-host "Fin du script d'installation de l'hote $env:COMPUTERNAME"