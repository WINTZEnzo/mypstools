# Partie configuration Windows
## Prérequis
Pour plus de simplicité pour l'administration des postes windows, un utilisateur ```ansible``` a été spécialement créé pour les futurs tâches d'aministration et ajouté au groupes d'administrateur depuis l'outil ```compmgmt.msc```.
## Setup Windows host
Pour rendre administrable un hôte sous Windows, il faudra exécuter le script suivant en lançant un powershell en tant qu'administrateur :
```bash
./Setup_WinRM_Windows.ps1
```
## Une erreur lors de l'exécution d'un script
Si vous avez une erreur comme quoi le script ne peut s'exécuter, il faudra taper la commande suivante et cliquez sur "Oui pour tous" : 
```bash
Set-ExecutionPolicy Unrestricted
```

## Verification WinRM 
Pour vérifier l'écoute des hostes WinRM
``bash 
winrm e winrm/config/listener
``` 
Pour voir l'état de la configuration : 
```bash
winrm get winrm/config
```

## Si le WinRM ne fonctionne toujours pas
Il faudra aller en local sur la machine et modifier les GPO nécessaire au chemin suivant : 
```Configuration Ordinateur/Modèles d'administration/Gestion à distance de windows(WinRM)```

Exemple d'un fichier de configuration winrm fonctionnel :
```bash
Config
    MaxEnvelopeSizekb = 500
    MaxTimeoutms = 60000
    MaxBatchItems = 32000
    MaxProviderRequests = 4294967295
    Client
        NetworkDelayms = 5000
        URLPrefix = wsman
        AllowUnencrypted = false
        Auth
            Basic = true
            Digest = true
            Kerberos = false
            Negotiate = true
            Certificate = true
            CredSSP = false
        DefaultPorts
            HTTP = 5985
            HTTPS = 5986
        TrustedHosts = *
    Service
        RootSDDL = O:NSG:BAD:P(A;;GA;;;BA)(A;;GR;;;IU)S:P(AU;FA;GA;;;WD)(AU;SA;GXGW;;;WD)
        MaxConcurrentOperations = 4294967295
        MaxConcurrentOperationsPerUser = 1500
        EnumerationTimeoutms = 240000
        MaxConnections = 300
        MaxPacketRetrievalTimeSeconds = 120
        AllowUnencrypted = true [Source="GPO"]
        Auth
            Basic = true
            Kerberos = false
            Negotiate = true
            Certificate = false
            CredSSP = false
            CbtHardeningLevel = Relaxed
        DefaultPorts
            HTTP = 5985
            HTTPS = 5986
        IPv4Filter = *
        IPv6Filter = *
        EnableCompatibilityHttpListener = false
        EnableCompatibilityHttpsListener = false
        CertificateThumbprint
        AllowRemoteAccess = true
    Winrs
        AllowRemoteShellAccess = true
        IdleTimeout = 7200000
        MaxConcurrentUsers = 2147483647
        MaxShellRunTime = 2147483647
        MaxProcessesPerShell = 2147483647
        MaxMemoryPerShellMB = 2147483647
        MaxShellsPerUser = 2147483647
```